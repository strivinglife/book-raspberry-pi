# Raspberry Pi 2 Models

All models are the Pi 2B.

1. Unopened
2. 1.1 (Revision a01041): NAS
	- [Setting up a Raspberry Pi 2 Samba server (in early January 2016)](https://words.strivinglife.com/post/setting-up-a-raspberry-pi-2-samba-server-in-early-january-2016/)
3. 1.1 (Revision a21041): Retro gaming?
