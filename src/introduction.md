# Introduction
The following book includes notes regarding my personal Raspberry Pi collection.

Public benefit may be limited.

## Pages of Note
- [Raspberry Pi 2 Models](./pi2/models.md)
- [Raspberry Pi 3 Models](./pi3/models.md)
- [Raspberry Pi 4 Models](./pi4/models.md)
