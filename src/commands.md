# Raspberry Pi Commands

## Current Uptime
```bash
uptime
```

## Restarting
```bash
sudo reboot
```

## Shutdown
```bash
sudo shutdown -h now
```

## Internet Configuration
```bash
ifconfig

# Try to bring a network interface up.
sudo ifup wlan0
```

## Samba
```bash
# Restart Samba services.
sudo /etc/init.d/samba restart

# Backup copy of Samba configuration.
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old
# Edit Samba configuration. Restart (see above) for changes to be picked up.
sudo nano /etc/samba/smb.conf

# Check Samba services.
systemctl status smbd
```

## SSH
```bash
# Check SSH service status.
systemctl status sshd

# SSH into a server with Raspberry Pi's default user.
# Enter `exit` and press enter to quit the session.
ssh pi@<ip_address>
```

## Terminal
```bash
# View past commands.
history

# View process information
top
```

## Drive Information
```bash
# List device information.
sudo fdisk -l

# List mounts.
sudo mount -l

# Example: cd into a mounted USB device.
cd /media/usbhdd1
```

## USB/Port Information
```bash
# Basic list of each port.
lsusb
# Lists out each port, with Product if there's a connected device.
usb-devices
```

## Display Information
```bash
# List information about the connected display.
tvservice -s
```

## Hardware Information
See https://elinux.org/RPi_HardwareHistory#Board_Revision_History for more information.
```bash
# Includes Hardware and Revision information.
cat /proc/cpuinfo
```

## OS Information
```bash
cat /etc/os-release
```

## RetroPie audio issues
1. Make sure the correct audio output device is selected in the RetroPie configuration.
2. `sudo nano /boot/config.txt` and uncomment `#hdmi_drive=2`. <kbd>ctrl+o</kbd>, <kbd>enter</kbd>, <kbd>ctrl+x</kbd>, `sudo reboot`.

## Start GUI
```bash
startx
```
