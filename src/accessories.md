# Accessories
This listing may include affiliate links to Amazon.com.

## Books
- [Raspberry Pi User Guide, Fourth Edition (review)](https://words.strivinglife.com/post/review-raspberry-pi-user-guide-fourth-edition/)

## Controllers
List alphabetically.

- [Atari Ultimate Arcade Fightstick USB Dual Joystick with Trackball 2 Player Game Controller Powered by Raspberry Pi 3B+](https://www.microcenter.com/product/623130/atari-ultimate-arcade-fightstick-usb-dual-joystick-with-trackball-2-player-game-controller-powered-by-raspberry-pi-3b?storeid=029)
	- More than just a controller, since it includes the Pi, but there's versions that are available without it.
	- I did swap out the buttons with [EG STARTS 6X Happ Type Standard Push Buttons with Micro Switch](https://amzn.to/30PriCz) so that I could get some different colors.
	- Also picked up some [Baolian Concave Button - Silver Chrome](https://www.microcenter.com/product/613155/baolian-concave-button-silver-chrome) and [Baolian brand](https://www.microcenter.com/brand/4294811327/baolian) one and two-player buttons (white and black).
	- Finally, put some [LoveRPi Performance Heatsink Set for Raspberry Pi 3 B+](https://amzn.to/3oLcBsl) heatsinks on the Pi itself.
- [Buffalo iBuffalo Classic USB Gamepad for PC](https://amzn.to/3CD4anQ)
	- These have gotten expensive since I picked them up in 2015 and 2016. Work well with RetroPie.
- [DOYO Arcade Stick Joystick for Home, Durable Iron Arcade Fightstick Compatible with PC/ PS3 / Switch/NEOGEO Mini/Android/Raspberry Pi](https://amzn.to/3DIRs8n)
	- Haven't plugged this in yet to try it out.
- [Pimoroni Picade Console Controller Kit (PIM106)](https://shop.pimoroni.com/products/picade-console)
	- Picked mine by through the [adafruit site, which has a newer version available](https://www.adafruit.com/product/2707).
	- Haven't written a full review, but the arcade buttons on this aren't that good.
- [Vilros Retro Gaming Classic USB Gamepad Set](https://vilros.com/products/vilros-retro-gaming-usb-classic-controller-set-of-5)
	- Out of stock everywhere, but may eventually come back to stock on their site (main link above) or [on Amazon](https://amzn.to/3oLiANH).
	- Picked up two sets of these back in 2019.
- [VOYEE PC Controller, Wired Game Controller Compatible with Microsoft Xbox 360 & Slim/PC Windows 10/8/7, with Upgraded Joystick, Double Shock | Enhanced (Black)](https://amzn.to/3qV9e4P)
	- Work great with PC and Raspberry Pi.

## Keyboards
Note that Amazon links to these point to different products than what I originally purchased. Looks like the product pages are just updated to point to their latest product offerings, and some might have been hijacked.

A. [ANEWISH 2.4GHz Mini Wireless Keyboard with Touchpad Mouse Combo, Rechargable Li-ion Battery](https://amzn.to/3nBYLZO)
	- From 2019.
B. Branded Tsing, but now pointing to [iPazzPort Wireless Mini Keyboard with Touchpad](https://amzn.to/3cw1sWG), I think.
	- Requires 2 AAA batteries.
	- From 2015.
C. [Rii 2.4GHz Mini Wireless Keyboard with Touchpad＆QWERTY Keyboard](https://amzn.to/3nAuyum)
	- From 2016.
D. [ANEWISH 2.4GHz RF Wireless Mini Keyboard with Touchpad Mouse Combo, Rechargable & Light](https://amzn.to/3DJc28E)
	- From 2021.
E. X1 model of the [Rii 2.4G Mini Wireless Keyboard with Touchpad Mouse,Lightweight Portable Wireless Keyboard Controller with USB Receiver](https://amzn.to/3kXtcrQ)
	- From 2016.

## Networking
- [TP-Link TL-SG105 | 5 Port Gigabit Unmanaged Ethernet Network Switch, Ethernet Splitter | Plug & Play | Fanless Metal Design | Shielded Ports | Traffic Optimization | Limited Lifetime Protection](https://www.amazon.com/gp/product/B00A128S24/?tag=strivinglifen-20)
- I've purchased a lot of [ZOSION brand Cat 8 Ethernet cable](https://amzn.to/3kUbiGc) to wire up my space.
	- I also have some stuff from HiiPeak and Dacrown.

## USB Hubs
- [Amazon Basics 4 Port USB to USB 3.0 Hub with 5V/2.5A power adapter](https://amzn.to/3CH3dL8)
	- Used to run by basic NAS. Picked up back in 2016 and still working fine in 2021, albeit with only one external drive connected in all that time.
- [Sabrent 4-Port USB 3.0 Hub with Individual LED Lit Power Switches, Includes 5V/2.5A Power Adapter (HB-UMP3)](https://amzn.to/3cC1cFD)
	- Hasn't been used in a project yet.
