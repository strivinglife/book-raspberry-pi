# Raspberry Pi 3 Models

I have 3B and 3B+ models of the Raspberry Pi 3.

## 3B
1. 1.2 (Revision a02082): Retro gaming
	- RetroPie 4.1.8
	- Atari 2600, GameBoy, GameBoy Color, Sega MasterSystem, Sega MegaDrive, NES, Sega 32X, SNES

## 3B+
1. 1.3 (Revision a020d3): Vilros
	- RetroPie 4.4
2. 1.3 (Revision a020d3): Atari Ultimate Arcade Fightstick USB Dual Joystick with Trackball 2 Player Game Controller Powered by Raspberry Pi 3B+
	- [Micro Center product page](https://www.microcenter.com/product/623130/atari-ultimate-arcade-fightstick-usb-dual-joystick-with-trackball-2-player-game-controller-powered-by-raspberry-pi-3b)
	- RetroPie 4.5.13
